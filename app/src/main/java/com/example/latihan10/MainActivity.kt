package com.example.latihan10

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.latihan10.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private var calcValue: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.btnReset.setOnClickListener { _ -> this.onReset() }
        binding.calcValue.text = this.calcValue.toString()
        binding.btnAddBy1.setOnClickListener { _ ->
            this.setCalcValue(1)
        }
        binding.btnAddBy2.setOnClickListener { _ ->
            this.setCalcValue(2)
        }
        binding.btnAddBy5.setOnClickListener { _ ->
            this.setCalcValue(5)
        }

        binding.btnMinBy1.setOnClickListener { _ -> this.minBy1() }
        binding.btnMinBy2.setOnClickListener { _ -> this.minBy2() }
        binding.btnMinBy5.setOnClickListener { _ -> this.minBy5() }
    }

    private fun setCalcValue(num: Int) {
        var tmp = this.calcValue + num
        this.calcValue = if (tmp <= 0) 0 else tmp
        binding.calcValue.text = this.calcValue.toString()
    }

    private fun onReset() {
        this.calcValue = 0
        binding.calcValue.text = this.calcValue.toString()
        Toast.makeText(applicationContext, "Nomor telah direset", Toast.LENGTH_LONG).show()
    }

    private fun minBy1() {
        if (this.calcValue >= 1) this.setCalcValue(-1)
    }

    private fun minBy2() {
        if (this.calcValue >= 2) this.setCalcValue(-2)
    }

    private fun minBy5() {
        if (this.calcValue >= 5) this.setCalcValue(-5)
    }
}